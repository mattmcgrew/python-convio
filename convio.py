﻿from __future__ import print_function, unicode_literals, division

import urllib
import urllib2
import logging
import json
import sys
import types

from urllib2 import HTTPError

"""
This module provides a class for interacting with the various Convio APIs.

Users will need to consult the Convio API documentation for the appropriate method call signatures.

Note: you may call Convio methods in camelCase as they are in the Convio documentation or you may use
the Python convention for method calls (camelCaseMethodCall would become camel_case_method_call) and
they will be camelCase before making the call to Convio.

Example:

    cons_user = constituent.get_user("client", cons_id=user.cons_id, success_redirect='/member_profile/')

    is the same as:

    cons_user = constituent.getUser("client", cons_id=user.cons_id, success_redirect='/member_profile/')
"""

# Used when you want to test without making the actual request to Convio
TEST_NO_CALL = False
DEBUG = False

# Some useful Convio error code constants
CONSTITUENT_RECORD_NOT_FOUND = 16
ADD_FAILED_DUPLICATE = 11
UPDATE_FAILED_DUPLICATE_USER_NAME = 12
UPDATE_FAILED_DUPLICATE_EMAIL = 13

# Setup some very basic logging for testing
logging.basicConfig(stream=sys.stdout, level='DEBUG')

# This really belongs in a settings file
POST_DEFAULTS = {
    'api_key': '<convio API key>',
    'v': '1.0',
    'login_name': '<convio login name>',
    'login_password': '<convio login password>',
    'response_format': 'json',
}

#  This should be move to settings file or somewhere else
API_URL = "https://secure3.convio.net/{site_id}/site/"


class ConvioApi(object):
    """
    Class that maps method calls to Convio API calls.

    Process and make calls to appropriate Convio API URL. If called with:

        some_api.some_method_call("server", some_var=3, some_other_var=True)

    it will be translated to the request URL:

        "https://secure2.convio.net/{ORG_NAME}/site/{Some server API name}?method=
            someMethodCall&some_var=3&some_other_var=True"

    The "Some server API name" is given when the ConvioApi is instantiated as the "server" variable.

    An example instantiation:

        advocacy = ConvioApi(server='SRAdvocacyAPI')

    Which would make the calls to Convio look like:

        "https://secure2.convio.net/{ORG_NAME}/site/SRAdvocacyAPI?method=someMethodCall&
            some_var=3&some_other_var=True"

    Now we can do this:

        cons_alerts = advocacy.get_advocacy_alerts(alert_type="ACTION", alert_status="DRAFT")
        cons_user = constituent.get_user("client", cons_id=user.cons_id, success_redirect='/member_profile/')
    """
    def ping(self):
        """Ping the convio server with a request that should always work."""
        try:
            # TODO: reliance on the constituent instance should be removed
            constituent.is_email_valid(email="nobody@nobody.com")
            return True
        except ConvioApiException:
            return False

    def __init__(self, defaults, url, server=None, client=None):
        self.server = server
        self.client = client
        self.defaults = defaults
        self.url = url

        # method name cache for storing camelized method calls
        self.method_names = {}

    def _api_call(self, api_type, **params):
        if api_type == 'client' and self.client:
            api = self.client
        elif api_type == 'server' and self.server:
            api = self.server
        else:
            raise AttributeError("There is no API defined for the specified type '{0}'.".format(api_type))

        url = self.url + api

        # add defaults to request parameters
        params.update(self.defaults)

        # TODO: rewrite using Requests (http://docs.python-requests.org/en/latest/)
        data = urllib.urlencode(params)
        request = urllib2.Request(url, data)

        if DEBUG:
            logging.debug("-"*79)
            logging.debug(params['method'])
            logging.debug(request.get_full_url())
            logging.debug(request.get_data())
            logging.debug("-"*79)

        if TEST_NO_CALL:
            return {'TEST_NO_CALL': TEST_NO_CALL}

        try:
            response = urllib2.urlopen(request)
            bare_response = response.read()
            logging.debug(bare_response)
            response_data = json.loads(bare_response)
        except HTTPError, e:
            if e.code == 403:
                # in Convio this means an error
                # occurred, not that it was really "forbidden"
                bare_response = e.read()
                logging.debug(bare_response)
                response_data = json.loads(bare_response)
                raise ConvioApiException(
                    int(response_data['errorResponse']['code']),
                    response_data['errorResponse']['message'],
                    response_data
                )
            else:
                raise ConvioApiException(e.code, e.msg)

        return response_data

    def _fix_method_name(self, name):
        # camel case the "_"ed method name
        # to make it compatible with Convio camel case
        if name in self.method_names:
            method = self.method_names[name]
        else:
            np = name.split("_")
            method = np[0] + "".join([np[x].capitalize() for x in range(1, len(np))])
            self.method_names[name] = method

        return method

    def __getattr__(self, name):
        """
        Catch unbound API calls, create a function to call the appropriate Convio API,
        and bind the new method to the instance.
        """
        # Raise an exception for dunders as other functions (like 'dir()') will try to access
        # various dunders that might not exist
        if name.startswith("__") and name.endswith("__"):
            raise AttributeError(name)

        def api_call(self, api_type="server", **params):
            """Dynamically creates the Convio API call by "name"."""
            params['method'] = self._fix_method_name(name)
            return self._api_call(api_type, **params)

        # bind the new API method to this instance
        setattr(self, name, types.MethodType(api_call, self))

        return getattr(self, name)


class ConvioApiException(Exception):
    """Exception for any errors encountered during the calls to Convio"""
    def __init__(self, errno=None, msg=None, raw_response=None):
        super(ConvioApiException, self).__init__(errno, msg)
        self.raw_response = raw_response
        self.message = msg
        self.code = errno

    def __unicode__(self):
        return "Convio API error response. Code: {0}; Message: {1}".format(self.code, self.message)

    def __str__(self):
        return unicode(self).encode('utf-8')


def make_api(**kwargs):
    """Factory to create API instances with some defaults"""
    return ConvioApi(*[POST_DEFAULTS, API_URL], **kwargs)

# Create some default API types for this module
advocacy = make_api(server='SRAdvocacyAPI')
constituent = make_api(server='SRConsAPI')
donation = make_api(client='CRDonationAPI')


if __name__ == '__main__':
    TEST_NO_CALL = True # sadly we no longer have an active Convio account to test with
    DEBUG = True

    cons_alerts = advocacy.get_advocacy_alerts(alert_type="ACTION", alert_status="DRAFT")
    cons_user = constituent.get_user("server", cons_id=1234, success_redirect='/member_profile/')
    cons_alerts = advocacy.get_advocacy_alerts(alert_type="ACTION", alert_status="DRAFT")

    print(cons_alerts)

    # test using camelCase method calls
    test = advocacy.justTestingThis(param1=100, param2="test")

    # Make a bunch of functions
    for x in range(100):
        test = getattr(advocacy, "just_testing_this_{0}".format(x))(param1=x, param2="test")


    print(advocacy.just_a_test)

    print(dir(advocacy))

    print(advocacy.method_names)

    TEST_NO_CALL = False

    # this should return False since we do not have access to Convio
    assert advocacy.ping() is False
